Summary
-------

Social Share Count displays the total number of social shares for a page. The
share count is currently comprised of the following social networks:

- Facebook
- Google+
- LinkedIn
- Twitter


Installation
------------

1. Download and enable the module.

2. Optionally, configure the module under:

   Admin > Configuration > Web Services > Social Share Count

3. Print the share count for an absolute URL with the following PHP expression:

   <?php print theme('social_share_count', array(
     'url' => 'http://www.example.com',
   )); ?>

   Example:

   To display the share count on every page, insert the following piece of code
   in your html.tpl.php template file:

   <?php print theme('social_share_count', array(
     'url' => url(current_path(), array('absolute' => TRUE))
   )); ?>