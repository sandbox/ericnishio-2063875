<?php

/**
 * Class SocialShareCount
 * @author Eric Nishio <eric@self-learner.com>
 */
class SocialShareCount {
  protected $url;
  protected $facebook    = 0;
  protected $twitter     = 0;
  protected $googlePlus  = 0;
  protected $linkedIn    = 0;
  protected $timeout     = 10;

  // Prefix for cached entries
  const CACHE_PREFIX = 'social_share_count__';

  // Social network constants
  const FACEBOOK = 'Facebook';
  const TWITTER = 'Twitter';
  const LINKEDIN = 'LinkedIn';
  const GOOGLEPLUS = 'Google+';

  // API URL constants
  const FACEBOOK_API_URL = 'http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls=';
  const TWITTER_API_URL = 'http://urls.api.twitter.com/1/urls/count.json?url=';
  const LINKEDIN_API_URL = 'http://www.linkedin.com/countserv/count/share?format=json&url=';
  const GOOGLEPLUS_API_URL = 'https://clients6.google.com/rpc';

  /**
   * The constructor method.
   * @param string $url the page URL.
   */
  public function __construct($url) {
    $this->url = rawurldecode($url);
    $this->twitter = $this->getTwitter();
    $this->facebook = $this->getFacebook();
    $this->linkedIn = $this->getLinkedIn();
    $this->googlePlus = $this->getGooglePlus();
  }

  /**
   * Returns the total share count.
   * @returns integer the total share count.
   */
  public function getTotal() {
    return $this->facebook + $this->twitter + $this->googlePlus + $this->linkedIn;
  }

  /**
   * Retrieves the share count on Facebook.
   * @returns integer the share count.
   */
  protected function getFacebook() {
    $shares = 0;
    $likes = 0;
    $includeLikes = (int) variable_get('social_share_count_include_facebook_likes') === 1 ? TRUE : FALSE;
    $data = $this->httpRequest(self::FACEBOOK_API_URL . $this->url, self::FACEBOOK);

    if (isset($data[0])) {
      $data = $data[0];
      $shares = (isset($data->share_count) && $data->share_count > 0) ? $data->share_count : 0;
      $likes = ($includeLikes && isset($data->like_count) && $data->like_count > 0) ? $data->like_count : 0;
    }

    return $shares + $likes;
  }

  /**
   * Retrieves the share count on Twitter.
   * @returns integer the share count.
   */
  protected function getTwitter() {
    $count = 0;
    $data = $this->httpRequest(self::TWITTER_API_URL . $this->url, self::TWITTER);

    if (isset($data->count) && $data->count > 0) {
      $count = $data->count;
    }

    return $count;
  }

  /**
   * Retrieves the share count on Google+.
   * @returns integer the share count.
   */
  protected function getGooglePlus() {
    $count = 0;
    $data = $this->httpRequest($this->url, self::GOOGLEPLUS);

    if (isset($data[0]) && isset($data[0]->result) && $data[0]->result->metadata->globalCounts->count > 0) {
      $count = (int) $data[0]->result->metadata->globalCounts->count;
    }

    return $count;
  }

  /**
   * Retrieves the share count on LinkedIn.
   * @returns integer the share count.
   */
  protected function getLinkedIn() {
    $count = 0;
    $data = $this->httpRequest(self::LINKEDIN_API_URL . $this->url, self::LINKEDIN);

    if (isset($data->count) && $data->count > 0) {
      $count = $data->count;
    }

    return $count;
  }

  /**
   * Performs an HTTP request to a given URL.
   * @param string $url the URL.
   * @param string $socialNetwork the name of the social network.
   * @return string the URL contents.
   */
  protected function httpRequest($url, $socialNetwork) {
    $targetUrl = $url; // save the target URL
    $cache_expire = variable_get('social_share_count_cache_expire', 600);
    $cached = cache_get(self::CACHE_PREFIX . $targetUrl);

    if ($cache_expire > 0 && $cached) {
      $result = $cached->data;
    }
    else {
      // Default HTTP request options
      $method = 'GET';
      $data = NULL;
      $headers = array();

      // HTTP request options for Google+
      if ($socialNetwork === self::GOOGLEPLUS) {
        $headers['Content-Type'] = 'application/json';
        $method = 'POST';
        $data = json_encode(array(array(
          'method' => 'pos.plusones.get',
          'id' => 'p',
          'params' => array(
            'nolog' => true,
            'id' => rawurldecode($url),
            'source' => 'widget',
            'userId' => '@viewer',
            'groupId' => '@self',
          ),
          'jsonrpc' => '2.0',
          'key' => 'p',
          'apiVersion' => 'v1',
        )));

        // Override URL
        $url = self::GOOGLEPLUS_API_URL;
      }

      // Set the HTTP request options
      $options = array(
        'headers' => $headers,
        'method' => $method,
        'data' => $data,
        'timeout' => $this->timeout,
      );

      // Get HTTP response
      $response = drupal_http_request($url, $options);

      // Set error messages
      if (isset($response->error)) {
        drupal_set_message(t('Social Share Count Error (@socialNetwork): @error', array(
          '@error' => $response->error,
          '@socialNetwork' => $socialNetwork,
        )), 'error');
      }

      // Decode JSON data
      $result = isset($response->data) ? json_decode($response->data) : NULL;

      // Cache the result
      if ($cache_expire > 0 && !empty($result)) {
        cache_set(self::CACHE_PREFIX . $targetUrl, $result, 'cache', time() + $cache_expire);
      }
    }

    return $result;
  }
}